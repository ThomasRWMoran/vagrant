apt -y update

apt -y install nginx

cp /vagrant/files/index.html /usr/share/nginx/html/index.html
cp /vagrant/files/ubuntulinkedpage.html /usr/share/nginx/html/linkedpage.html

update-rc.d nginx enable
service nginx start

ifconfig | grep inet | grep -v 127.0.0.1 | grep -v inet6 | grep -v 10.0.2.15
