#!/bin/bash

yum -y install httpd

cat >/var/www/html/index.html <<_END_
<html>
<head>
<title>Hello Vagrant</title>
</head>
<body>
<h1>Hello from RHEL Vagrant</h1>
<p>You are on $HOSTNAME</p>
<p>$(ls -l)</p>
<a href="linkedpage.html">This is a link I think</a>
</body>
</html>
_END_

cp /vagrant/files/linkedpage.html /var/www/html/linkedpage.html

chown root:root /var/www/html/index.html
systemctl start httpd #starts Apache
systemctl enable httpd #enables Apache
ifconfig | grep inet | grep -v 127.0.0.1 | grep -v inet6 | grep -v 10.0.2.15
